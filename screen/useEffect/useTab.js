// import React, { useState, useEffect } from "react";

// export default function useTab({ focused }) {
//   //   const [color, setColor] = useState();
//   const [size, setSize] = useState(22);

//   useEffect(() => {
//     // setColor(focused ? "#3D0069" : "#694FAD");
//     setSize(focused ? 30 : 22);
//   }, [focused]);
//   return size;
// }
import { useEffect } from "react";
function Greet({ name }) {
  const message = `Hello, ${name}!`;
  useEffect(() => {
    // Runs once, after mounting
    document.title = "Greetings page";
  }, []);
  return <div>{message}</div>;
}
