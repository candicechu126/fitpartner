// React Native Navigation Drawer – Example using Latest Navigation Version //
// https://aboutreact.com/react-native-navigation-drawer //
import * as React from "react";
import { Button, View, Text, SafeAreaView, ScrollView } from "react-native";

const About = ({ navigation }) => {
  return (
    <ScrollView>
      <View
        style={{
          fontSize: 18,
          textAlign: "center",
          margin: 10,
          alignItems: "center",
        }}
      >
        <Text>[SB05a-21]</Text>
        <Text>
          Self-proposed project on facial expression/gesture recognition
        </Text>
        <View
          style={{
            paddingTop: 10,
            alignItems: "left",
          }}
        >
          <Text>Supervisor: Shi Bertram</Text>
          <Text>Email: eebert@ust.hk</Text>
          <Text>Room: 2460, Phone: 7079</Text>
        </View>
      </View>
      <View
        style={{
          padding: 15,
        }}
      >
        <Text style={{ fontWeight: "bold" }}>Main Objective</Text>
        <Text>
          Main Objective The target of this project is to design and build a
          fitness training mobile application system for the general public,
          named “Power Fitness”. Using advanced image processing technology, the
          application will be able to provide real-time fitness tutorials and
          tailor-made advice which is similar to a real personal fitness coach.
          By recording the user exercising and grading the correctness of the
          user’s posture in-depth, the system would be able to incentivize the
          group who seldom do sports and to achieve a higher level of physical
          health through constant motivations and behaviour changes.
        </Text>
        <Text style={{ fontWeight: "bold", paddingTop: 15 }}>
          Objective Statements
        </Text>
        <Text style={{ paddingTop: 10 }}>
          1. To integrate the human pose estimation model on mobile devices
        </Text>
        <Text style={{ paddingTop: 10 }}>
          2.To evaluate the performance of human pose estimation
        </Text>
        <Text style={{ paddingTop: 10 }}>
          3. To design a user interface on React Native for cross-platform
          application development
        </Text>
        <Text style={{ paddingTop: 10 }}>
          4. To design and evaluate the workout scoring algorithm
        </Text>
        <Text style={{ paddingTop: 10 }}>
          5. To implement additional functionality such as display activity
          charts, To-do checklist and pop-up notifications to establish a
          human-centric design
        </Text>
        <Text style={{ paddingTop: 10 }}>
          6. To invite members in HKUST to try out the application in order to
          collect feedbacks
        </Text>
      </View>
    </ScrollView>
  );
};

export default About;
