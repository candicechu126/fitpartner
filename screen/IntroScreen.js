import React, { useState } from "react";
import { StyleSheet, View, Text, Image } from "react-native";
import AppIntroSlider from "react-native-app-intro-slider";
import { EvilIcons } from "@expo/vector-icons";
import Index from "./Index";

export default function IntroScreen({ navigation }) {
  const [showRealApp, setShowRealApp] = useState(false);
  const onDone = () => {
    setShowRealApp(true);
  };

  const renderNextButton = () => {
    return (
      <EvilIcons
        name="arrow-right"
        size={40}
        color="black"
        style={{ marginRight: 30 }}
      />
    );
  };

  const renderDoneButton = () => {
    return (
      <EvilIcons
        name="check"
        size={40}
        color="black"
        style={{ marginRight: 30 }}
      />
    );
  };
  const RenderItem = ({ item }) => {
    return (
      <View
        style={{
          flex: 1,
          backgroundColor: "#fff",
          alignItems: "center",
          justifyContent: "space-around",
          paddingBottom: 100,
        }}
      >
        <Text style={styles.introTitleStyle}>{item.title}</Text>
        <Image style={styles.introImageStyle} source={item.image} />
        <Text style={styles.introTextStyle}>{item.text}</Text>
      </View>
    );
  };

  return (
    <>
      {showRealApp ? (
        <Index />
      ) : (
        <AppIntroSlider
          data={slides}
          renderItem={RenderItem}
          renderNextButton={renderNextButton}
          onDone={onDone}
          renderDoneButton={renderDoneButton}
        />
      )}
    </>
  );
}

const styles = StyleSheet.create({
  introImageStyle: {
    width: 300,
    height: 200,
  },
  introTextStyle: {
    fontSize: 18,
    fontFamily: "Georgia",
    color: "black",
    textAlign: "center",
    paddingVertical: 30,
  },
  introTitleStyle: {
    fontSize: 25,
    fontFamily: "Georgia",
    color: "black",
    textAlign: "center",
    marginBottom: 16,
    fontWeight: "bold",
  },
});

const slides = [
  {
    key: "s1",
    text: "Action is the key to all success",
    title: "Fitness Anywhere",
    // image: {
    //   uri: "https://firebasestorage.googleapis.com/v0/b/hardy-palace-218707.appspot.com/o/welcomeImage1.JPG?alt=media&token=1f003f49-5072-4c6c-814d-3adb1b37ed50",
    // },
    image: require("../assets/component/welcomeImage1.jpg"),
  },
  {
    key: "s2",
    title: "Healthy Life",
    text: "Exercise is labor without weariness",
    // image: {
    //   uri: "https://firebasestorage.googleapis.com/v0/b/hardy-palace-218707.appspot.com/o/welcomeImage2.JPG?alt=media&token=aa709bd6-0156-420d-8f3f-9774ff3e0715",
    // },
    image: require("../assets/component/welcomeImage2.jpg"),
  },
  {
    key: "s3",
    title: "Save Time",
    text: "Tough times don’t last, Tough people do",
    // image: {
    //   uri: "https://firebasestorage.googleapis.com/v0/b/hardy-palace-218707.appspot.com/o/welcomeImage3.JPG?alt=media&token=c8a679fe-cfc9-4d1a-8c07-ae1c295027bf",
    // },
    image: require("../assets/component/welcomeImage3.jpg"),
  },
  {
    key: "s4",
    title: "Best Records",
    text: " Our bodies are our gardenss",
    // image: {
    //   uri: "https://firebasestorage.googleapis.com/v0/b/hardy-palace-218707.appspot.com/o/welcomeImage4.JPG?alt=media&token=9ead16e4-e28d-4e02-9e6a-836e293abba4",
    // },
    image: require("../assets/component/welcomeImage4.jpg"),
  },
  {
    key: "s5",
    title: "Reduce Injury",
    text: "No pain, no gain",
    // image: {
    //   uri: "https://firebasestorage.googleapis.com/v0/b/hardy-palace-218707.appspot.com/o/welcomeImage5.JPG?alt=media&token=4c99f1d2-ccde-4735-9107-c6ff8e67beb4",
    // },
    image: require("../assets/component/welcomeImage5.jpg"),
  },
  {
    key: "s6",
    title: "Easy to Learn",
    text: "Sweat is fat crying",
    // image: {
    //   uri: "https://firebasestorage.googleapis.com/v0/b/hardy-palace-218707.appspot.com/o/welcomeImage6.JPG?alt=media&token=8e0bdcec-ccaf-4591-881b-63edf96ac9e9",
    // },
    image: require("../assets/component/welcomeImage6.jpg"),
  },
];
