import React from "react";
import {
  StyleSheet,
  View,
  SafeAreaView,
  Text,
  Pressable,
  Button,
} from "react-native";

export default function EditProfile({ navigation }) {
  return <SafeAreaView style={styles.body}></SafeAreaView>;
}

const styles = StyleSheet.create({
  body: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  text: {
    fontSize: 40,
    fontWeight: "bold",
    margin: 10,
  },
});
