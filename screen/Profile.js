import React, { useState } from "react";
import {
  View,
  Text,
  Image,
  ScrollView,
  StyleSheet,
  SafeAreaView,
  TouchableOpacity,
  Alert,
} from "react-native";
import EditProfile from "./EditPart/EditProfile";
import { Ionicons } from "@expo/vector-icons";

export default function Profile({ navigation }) {
  const [userData, setUserData] = useState(null);
  const [editData, setEditData] = useState(false);
  const onPressHandler = () =>
    Alert.alert("Edit personal information", "", [
      {
        text: "Cancel",
        onPress: () => setEditData(false),
        style: "cancel",
      },
      { text: "OK", onPress: () => setEditData(true) },
    ]);

  return editData ? (
    <EditProfile />
  ) : (
    <SafeAreaView style={{ marginBottom: 20 }}>
      <Image
        source={require("../assets/fitness-bg.jpeg")}
        style={{ padding: 110, width: "100%", height: "20%" }}
      />
      <View style={{ alignItems: "center" }}>
        <Image
          source={require("../assets/appBg.png")}
          style={{
            width: 140,
            height: 140,
            borderRadius: 100,
            marginTop: -70,
          }}
        />
        <Text
          style={{
            fontWeight: "bold",
            padding: 10,
            fontSize: 25,
            fontFamily: "Georgia",
          }}
        >
          Final Year Project
        </Text>
        <Text
          style={{
            fontWeight: "bold",
            color: "#737373",
            fontSize: 15,
            fontFamily: "Georgia",
          }}
        >
          fyp@connect.ust.hk
        </Text>
        <TouchableOpacity onPress={onPressHandler}>
          <Ionicons name="create" size={25} color="#818181"></Ionicons>
        </TouchableOpacity>
      </View>
      <ScrollView style={{ margin: 5, height: "45%" }}>
        <View style={styles.target}>
          <Ionicons name="nutrition-outline" size={30} color="#A090BA" />
          <Text style={styles.targetText}>Eat healthy</Text>
        </View>
        <View style={styles.target}>
          <Ionicons name="barbell-outline" size={30} color="#A090BA" />
          <Text style={styles.targetText}>Get regular exercise</Text>
        </View>
        <View style={styles.target}>
          <Ionicons name="water" size={30} color="#A090BA" />
          <Text style={styles.targetText}>Drink water</Text>
        </View>
        <View style={styles.target}>
          <Ionicons name="wine-outline" size={30} color="#A090BA" />
          <Text style={styles.targetText}>
            Limit how much alcohol you drink
          </Text>
        </View>
        <View style={styles.target}>
          <Ionicons name="happy" size={30} color="#A090BA" />

          <Text style={styles.targetText}>Be active and relax</Text>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  info: {
    flex: 1,
    paddingTop: 10,
    flexDirection: "column",
    backgroundColor: "#F0EDF6",
    justifyContent: "center",
    alignItems: "center",
    height: 250,
  },
  userImg: {
    height: 80,
    width: 80,
    borderRadius: 75,
  },
  userName: {
    flex: 1,
    fontSize: 18,
    fontFamily: "Georgia",
    fontWeight: "bold",
    marginTop: 10,
    marginBottom: 10,
  },
  separator: {
    marginVertical: 8,
    borderBottomColor: "#A090BA",
    borderBottomWidth: StyleSheet.hairlineWidth,
  },
  target: {
    alignSelf: "center",
    flexDirection: "row",
    justifyContent: "center",
    backgroundColor: "#fff",
    width: "90%",
    padding: 15,
    paddingBottom: 20,
    borderRadius: 10,
    shadowOpacity: 10,
    shadowColor: "#F0EDF6",
    elevation: 15,
    marginTop: 20,
  },
  targetText: {
    fontSize: 15,
    fontFamily: "Georgia",
    color: "#A090BA",
    fontWeight: "bold",
    marginLeft: 15,
    marginTop: 8,
  },
});
