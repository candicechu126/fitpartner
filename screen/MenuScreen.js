import * as React from "react";
import { NavigationContainer } from "@react-navigation/native";
import {
  createDrawerNavigator,
  DrawerContentScrollView,
} from "@react-navigation/drawer";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import {
  NativeBaseProvider,
  Box,
  Pressable,
  VStack,
  Text,
  HStack,
  Divider,
  Icon,
} from "native-base";
import Home from "./screen/Home";
import About from "./screen/About";
import Setting from "./screen/EditPart/Setting";

const Drawer = createDrawerNavigator();

const getIcon = (screenName) => {
  switch (screenName) {
    case "Home":
      return "home";
    case "About":
      return "cellphone-information";
    case "Setting":
      return "cog-outline";
    default:
      return undefined;
  }
};

function CustomDrawerContent(props) {
  return (
    <DrawerContentScrollView {...props} safeArea>
      <VStack space="6" my="2" mx="1">
        <Box px="4">
          <Text bold color="#616161">
            FYP
          </Text>
          <Text fontSize="14" mt="1" color="#9E9E9E" fontWeight="500">
            fyp21@gmail.com
          </Text>
        </Box>
        <VStack divider={<Divider />} space="4">
          <VStack space="3">
            {props.state.routeNames.map((name, index) => (
              <Pressable
                px="5"
                py="3"
                rounded="md"
                bg={index === props.state.index ? "#FFEBCD" : "transparent"}
                onPress={(event) => {
                  props.navigation.navigate(name);
                }}
              >
                <HStack space="7" alignItems="center">
                  <Icon
                    color={index === props.state.index ? "#FF4800" : "#616161"}
                    size="5"
                    as={<MaterialCommunityIcons name={getIcon(name)} />}
                  />
                  <Text
                    fontWeight="500"
                    color={index === props.state.index ? "#FF4800" : "#616161"}
                  >
                    {name}
                  </Text>
                </HStack>
              </Pressable>
            ))}
          </VStack>
        </VStack>
      </VStack>
    </DrawerContentScrollView>
  );
}
function MyDrawer() {
  return (
    <Drawer.Navigator
      drawerContentOptions={{
        activeTintColor: "#e91e63",
        itemStyle: { marginVertical: 5 },
      }}
      screenOptions={{
        drawerStyle: {
          backgroundColor: "#fff1e6",
        },
        drawerActiveTintColor: "#FF4800",
      }}
      drawerContent={(props) => <CustomDrawerContent {...props} />}
    >
      <Drawer.Screen name="Home" component={Home} />
      <Drawer.Screen name="About" component={About} />
      <Drawer.Screen name="Setting" component={Setting} />
    </Drawer.Navigator>
  );
}
export default function App() {
  return (
    <NavigationContainer>
      <NativeBaseProvider>
        <MyDrawer />
      </NativeBaseProvider>
    </NavigationContainer>
  );
}

// // React Native Navigation Drawer – Example using Latest Navigation Version //
// // https://aboutreact.com/react-native-navigation-drawer //
// import "react-native-gesture-handler";
// import * as React from "react";
// import {
//   Button,
//   View,
//   Text,
//   TouchableOpacity,
//   Image,
//   ScrollView,
//   Box,
//   NativeBaseProvider,
// } from "react-native";
// import { VStack } from "native-base";
// import { NavigationContainer } from "@react-navigation/native";
// import { createStackNavigator } from "@react-navigation/stack";
// import { createDrawerNavigator } from "@react-navigation/drawer";

// import Src1 from "./screen/Scr1";
// import Src2 from "./screen/Scr2";
// import ThirdPage from "./screen/Scr3";
// import Setting from "./screen/EditPart/Setting";

// const Stack = createStackNavigator();
// const Drawer = createDrawerNavigator();

// const NavigationDrawerStructure = (props) => {
//   //Structure for the navigatin Drawer
//   const toggleDrawer = () => {
//     //Props to open/close the drawer
//     props.navigationProps.toggleDrawer();
//   };

//   return (
//     <View style={{ flexDirection: "row" }}>
//       <TouchableOpacity onPress={() => toggleDrawer()}>
//         {/*Donute Button Image */}
//         <Image
//           style={{
//             width: 25,
//             height: 25,
//             marginLeft: 5,
//             tintColor: "#e91e63",
//           }}
//           source={{
//             uri: "https://raw.githubusercontent.com/AboutReact/sampleresource/master/drawerWhite.png",
//           }}
//         />
//       </TouchableOpacity>
//     </View>
//   );
// };

// // function firstScreenStack({ navigation }) {
// //   return (
// //     <Stack.Navigator initialRouteName="FirstPage">
// //       <Stack.Screen
// //         name="FirstPage"
// //         component={FirstPage}
// //         options={{
// //           title: "First Page", //Set Header Title
// //           headerLeft: () => (
// //             <NavigationDrawerStructure navigationProps={navigation} />
// //           ),
// //           headerStyle: {
// //             backgroundColor: "#FF4800", //Set Header color
// //           },
// //           headerTintColor: "#fff", //Set Header text color
// //           headerTitleStyle: {
// //             fontWeight: "bold", //Set Header text style
// //           },
// //         }}
// //       />
// //     </Stack.Navigator>
// //   );
// // }

// // function secondScreenStack({ navigation }) {
// //   return (
// //     <Stack.Navigator
// //       // initialRouteName="SecondPage"
// //       screenOptions={{
// //         headerLeft: () => (
// //           <NavigationDrawerStructure navigationProps={navigation} />
// //         ),
// //         headerStyle: {
// //           backgroundColor: "#f4511e", //Set Header color
// //         },
// //         headerTintColor: "#fff", //Set Header text color
// //         headerTitleStyle: {
// //           fontWeight: "bold", //Set Header text style
// //         },
// //       }}
// //     >
// //       <Stack.Screen
// //         name="Src2"
// //         component={Src2}
// //         options={{
// //           title: "Second Page", //Set Header Title
// //         }}
// //       />
// //       <Stack.Screen
// //         name="ThirdPage"
// //         component={ThirdPage}
// //         options={{
// //           title: "Third Page", //Set Header Title
// //         }}
// //       />
// //     </Stack.Navigator>
// //   );
// // }

// function App() {
//   return (
//     <NavigationContainer>
//       <Drawer.Navigator
//         drawerContentOptions={{
//           activeTintColor: "#e91e63",
//           itemStyle: { marginVertical: 5 },
//         }}
//         screenOptions={{
//           drawerStyle: {
//             backgroundColor: "#fff1e6",
//           },
//           drawerActiveTintColor: "#FF4800",
//         }}
//       >
//         <Drawer.Screen
//           name="Home"
//           options={{ drawerLabel: "Home" }}
//           component={Src1}
//         />
//         <Drawer.Screen
//           name="Src2"
//           options={{ drawerLabel: "Second page Option" }}
//           component={Src2}
//         />
//         <Drawer.Screen
//           name="Setting"
//           options={{ drawerLabel: "Setting" }}
//           component={Setting}
//         />
//       </Drawer.Navigator>
//     </NavigationContainer>
//   );
// }

// export default App;
