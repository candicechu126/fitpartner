// // React Native Navigation Drawer – Example using Latest Navigation Version //
// // https://aboutreact.com/react-native-navigation-drawer //
// import React, { useState } from "react";
// import {
//   Button,
//   View,
//   Text,
//   SafeAreaView,
//   StyleSheet,
//   TouchableOpacity,
// } from "react-native";
// import { Ionicons } from "react-native-vector-icons";
// import useTab from "./useEffect/useTab";

// const Scr1 = ({ navigation }) => {
//   const [focusedWorkout, setFocusedWorkout] = useState(true);
//   const [focusedProfile, setFocusedProfile] = useState(false);
//   const [focusedStatistic, setFocusedStatistic] = useState(false);
//   const [focusedRecord, setFocusedRecord] = useState(false);

//   const handleWorkout = () => {
//     setFocusedWorkout(true),
//       setFocusedProfile(false),
//       setFocusedStatistic(false),
//       setFocusedRecord(false);
//   };

//   const handleProfile = () => {
//     setFocusedWorkout(false),
//       setFocusedProfile(true),
//       setFocusedStatistic(false),
//       setFocusedRecord(false);
//   };
//   const handleStatistic = () => {
//     setFocusedWorkout(false),
//       setFocusedProfile(false),
//       setFocusedStatistic(true),
//       setFocusedRecord(false);
//   };
//   const handleRecord = () => {
//     setFocusedWorkout(false),
//       setFocusedProfile(false),
//       setFocusedStatistic(false),
//       setFocusedRecord(true);
//   };
//   console.log("hi vghj", <useTab name="Eric" />);
//   return (
//     <View style={{ flex: 12 }}>
//       <View style={{ flex: 12, padding: 5 }}>
//         <View
//           style={{
//             flex: 1,
//             alignItems: "center",
//             justifyContent: "center",
//           }}
//         >
//           <Button
//             onPress={() => navigation.navigate("SecondPage")}
//             title="Go to Second Page"
//           />
//           <Button
//             onPress={() => navigation.navigate("ThirdPage")}
//             title="Go to Third Page"
//           />
//         </View>
//       </View>
//       <View style={styles.inBottomTab}>
//         <TouchableOpacity style={styles.bottomStyle} onPress={handleWorkout}>
//           <Ionicons
//             name="flame-outline"
//             size={focusedWorkout ? 30 : 22}
//             color={focusedWorkout ? "#3D0069" : "#694FAD"}
//           ></Ionicons>
//           <Text
//             style={
//               (styles.inBottomText,
//               { color: focusedWorkout ? "#3D0069" : "#694FAD" })
//             }
//           >
//             Workout
//           </Text>
//         </TouchableOpacity>
//         <TouchableOpacity style={styles.bottomStyle} onPress={handleProfile}>
//           <Ionicons
//             name="person-outline"
//             size={focusedProfile ? 30 : 22}
//             color={focusedProfile ? "#3D0069" : "#694FAD"}
//           ></Ionicons>
//           <Text
//             style={
//               (styles.inBottomText,
//               { color: focusedProfile ? "#3D0069" : "#694FAD" })
//             }
//           >
//             Profile
//           </Text>
//         </TouchableOpacity>
//         <TouchableOpacity style={styles.bottomStyle} onPress={handleStatistic}>
//           <Ionicons
//             name="analytics-outline"
//             size={focusedStatistic ? 30 : 22}
//             color={focusedStatistic ? "#3D0069" : "#694FAD"}
//           ></Ionicons>
//           <Text
//             style={
//               (styles.inBottomText,
//               { color: focusedStatistic ? "#3D0069" : "#694FAD" })
//             }
//           >
//             Statistic
//           </Text>
//         </TouchableOpacity>
//         <TouchableOpacity style={styles.bottomStyle} onPress={handleRecord}>
//           <Ionicons
//             name="bookmarks-outline"
//             size={focusedRecord ? 30 : 22}
//             color={focusedRecord ? "#3D0069" : "#694FAD"}
//           ></Ionicons>
//           <Text
//             style={
//               (styles.inBottomText,
//               { color: focusedRecord ? "#3D0069" : "#694FAD" })
//             }
//           >
//             Record
//           </Text>
//         </TouchableOpacity>
//       </View>
//     </View>
//   );
// };

// export default Scr1;

// const styles = StyleSheet.create({
//   bottomStyle: {
//     flex: 1,
//     flexDirection: "column",
//     alignItems: "center",
//     paddingTop: 5,
//   },
//   inBottomTab: {
//     flex: 1,
//     flexDirection: "row",
//     backgroundColor: "#fafffd",
//     paddingBottom: 15,
//     alignItems: "center",
//   },
//   inBottomText: {
//     fontFamily: "Georgia",
//     paddingTop: 3,
//   },
// });

import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { Ionicons } from "react-native-vector-icons";
import Workout from "./WorkoutSelectPage";
import Statistic from "./Statistic";
import Record from "./Record";
import Profile from "./Profile";
const Tab = createBottomTabNavigator();

export default function Home({ navigation }) {
  return (
    <Tab.Navigator
      initialRouteName="Workout"
      screenOptions={{
        tabBarActiveTintColor: "#7C4DFF",
        headerShown: false,
      }}
    >
      <Tab.Screen
        name="Workout"
        component={Workout}
        listeners={{
          tabPress: () => navigation.setOptions({ title: "Workout" }),
        }}
        options={{
          tabBarLabel: "Workout",
          tabBarIcon: ({ color, size }) => (
            <Ionicons name="flame-outline" color={color} size={size} />
          ),
        }}
      />
      <Tab.Screen
        name="Statistic"
        component={Statistic}
        listeners={{
          tabPress: () => navigation.setOptions({ title: "Statistic" }),
        }}
        options={{
          tabBarLabel: "Statistic",
          tabBarIcon: ({ color, size }) => (
            <Ionicons name="analytics-outline" color={color} size={size} />
          ),
        }}
      />

      <Tab.Screen
        name="Profile"
        component={Profile}
        listeners={{
          tabPress: () => navigation.setOptions({ title: "Profile" }),
        }}
        options={{
          tabBarLabel: "Profile",
          tabBarIcon: ({ color, size }) => (
            <Ionicons name="person-outline" color={color} size={size} />
          ),
        }}
      />
      <Tab.Screen
        name="Record"
        component={Record}
        listeners={{
          tabPress: () => navigation.setOptions({ title: "Record" }),
        }}
        options={{
          tabBarLabel: "Record",
          tabBarIcon: ({ color, size }) => (
            <Ionicons name="bookmarks-outline" color={color} size={size} />
          ),
        }}
      />
    </Tab.Navigator>
  );
}
