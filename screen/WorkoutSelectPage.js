import { Center } from "native-base";
import React, { useState } from "react";
import {
  Image,
  FlatList,
  SafeAreaView,
  StatusBar,
  StyleSheet,
  View,
  Text,
  TextInput,
  TouchableOpacity,
  Alert,
  Button,
  Modal,
} from "react-native";
import AccessCamera from "./AccessCamera";

const Item = ({ item, onPress, backgroundColor, textColor }) => (
  <TouchableOpacity onPress={onPress} style={[styles.item, backgroundColor]}>
    <Image style={styles.image} source={item.igurl} />
    <Text style={[styles.title, textColor]}>{item.title}</Text>
  </TouchableOpacity>
);

export default function WorkoutSelectPage() {
  const [searchInput, setSearchinput] = useState("");
  const [selectedId, setSelectedId] = useState(null);
  const [currentData, setCurrentData] = useState(data);
  const [openCamera, setOpenCamera] = useState(false);

  const handleSearch = (val) => {
    setSearchinput(val);
    setCurrentData(
      data.filter((item) =>
        item.title.toLowerCase().includes(val.toLowerCase())
      )
    );
  };

  const renderItem = ({ item }) => {
    const backgroundColor = item.id === selectedId ? "#A090BA" : "#F0EDF6";
    const color = item.id === selectedId ? "#fff" : "#58006A";
    const img = item.igurl;
    return (
      <Item
        item={item}
        onPress={() => {
          setSelectedId(item.id),
            Alert.alert(
              '"Fitness Partner" Would Like to Access the Camera',
              "This will let you take record video",
              [
                {
                  text: "Block",
                  onPress: () => console.log("Cancel Pressed"),
                  style: "cancel",
                },
                {
                  text: "Allow",
                  onPress: () => setOpenCamera(true),
                },
              ]
            );
        }}
        backgroundColor={{ backgroundColor }}
        ImageBackground={img}
        textColor={{ color }}
      />
    );
  };

  return (
    <View style={styles.mainView}>
      <Modal
        animationType="slide"
        // transparent={true}
        visible={openCamera}
        onRequestClose={() => {
          Alert.alert("Camera has been closed.");
          this.setModalVisible(!openCamera);
        }}
      >
        <View style={styles.buttonCloseCamera}>
          <Button title="Close camera" onPress={() => setOpenCamera(false)} />
        </View>
        <AccessCamera />
      </Modal>

      <Text style={styles.heading}>Fitness WorkOut</Text>
      <View style={styles.textInputView}>
        <TextInput
          value={searchInput}
          onChangeText={handleSearch}
          placeholder={"Enter Workout name"}
          placeholderTextColor={"#A4A2A2"}
          style={styles.textInput}
        />
      </View>
      <SafeAreaView style={styles.container}>
        <FlatList
          data={currentData}
          renderItem={renderItem}
          keyExtractor={(item) => item.id}
          extraData={selectedId}
        />
      </SafeAreaView>
    </View>
  );
}

const styles = StyleSheet.create({
  mainView: {
    flex: 1,
    backgroundColor: "#fff",
  },
  heading: {
    fontSize: 26,
    fontFamily: "Georgia",
    fontWeight: "bold",
    marginTop: 12,
    marginLeft: 12,
  },
  textInput: {
    height: 39,
    width: "90%",
    backgroundColor: "#F0EDF6",
    borderRadius: 20,
    paddingLeft: 15,
    marginTop: 12,
    marginBottom: 12,
  },
  textInputView: {
    display: "flex",
    alignItems: "center",
  },
  container: {
    flex: 1,
    marginTop: StatusBar.currentHeight || 0,
  },
  item: {
    padding: 20,
    height: 250,
    marginVertical: 14,
    marginHorizontal: 14,
    borderRadius: 15,
    shadowColor: "#512757",
    shadowOffset: {
      width: 2,
      height: 3,
    },
    shadowOpacity: 0.5,
    shadowRadius: 3.5,
    elevation: 8,
  },
  image: {
    width: 250,
    height: 200,
    marginHorizontal: 60,
  },
  title: {
    fontSize: 18,
    fontFamily: "Georgia",
  },
  buttonCloseCamera: {
    paddingTop: 45,
    alignItems: "flex-start",
  },
});

const data = [
  {
    id: "001",
    title: "Jumping Jacks",
    igurl: {
      uri: "https://firebasestorage.googleapis.com/v0/b/hardy-palace-218707.appspot.com/o/1-Jumping-jacks.png?alt=media&token=1504aa2d-c074-4e7a-9b6c-85e89d3ddac8",
    },
  },
  {
    id: "002",
    title: "Squat Exercise Illustration",
    igurl: {
      uri: "https://firebasestorage.googleapis.com/v0/b/hardy-palace-218707.appspot.com/o/2-squat-exercise-illustration.png?alt=media&token=68b40baa-5c5c-4340-9874-0672f7b6ef9b",
    },
  },
  {
    id: "003",
    title: "Side Leg Raise",
    igurl: {
      uri: "https://firebasestorage.googleapis.com/v0/b/hardy-palace-218707.appspot.com/o/3-Side-leg-raise.png?alt=media&token=941505c1-00a9-4dd8-84b8-37f6f502c392",
    },
  },
  {
    id: "004",
    title: "Donkey Kick Pulses",
    igurl: {
      uri: "https://firebasestorage.googleapis.com/v0/b/hardy-palace-218707.appspot.com/o/4-Donkey-Kick-pulses.png?alt=media&token=841e7362-818f-4537-b99e-0f9ddb5fd486",
    },
  },
  {
    id: "005",
    title: "Side Lunge Exercise Illustration",
    igurl: {
      uri: "https://firebasestorage.googleapis.com/v0/b/hardy-palace-218707.appspot.com/o/5-side-lunge-exercise-illustration.png?alt=media&token=50a28b05-82a7-496c-b65b-619c6e6c617a",
    },
  },
  {
    id: "006",
    title: "High Knees",
    igurl: {
      uri: "https://firebasestorage.googleapis.com/v0/b/hardy-palace-218707.appspot.com/o/6-High-Knees.png?alt=media&token=d799b165-ad16-4946-b933-d6efc83433af",
    },
  },
  {
    id: "007",
    title: "Plank",
    igurl: {
      uri: "https://firebasestorage.googleapis.com/v0/b/hardy-palace-218707.appspot.com/o/7-Plank.png?alt=media&token=be3cf6c0-8356-4970-83fb-2e9180074ed5",
    },
  },
  {
    id: "008",
    title: "Russian Twist",
    igurl: {
      uri: "https://firebasestorage.googleapis.com/v0/b/hardy-palace-218707.appspot.com/o/8-Russian-twist.png?alt=media&token=4bc81176-f9ed-4277-bb27-935646ab8b77",
    },
  },
  {
    id: "009",
    title: "Marching Glute Bridge",
    igurl: {
      uri: "https://firebasestorage.googleapis.com/v0/b/hardy-palace-218707.appspot.com/o/9-Marching-glute-bridge.png?alt=media&token=e31682aa-1d4d-4b74-a17c-12ca4ee3a2c8",
    },
  },
  {
    id: "010",
    title: "Lying Windshield Wipers",
    igurl: {
      uri: "https://firebasestorage.googleapis.com/v0/b/hardy-palace-218707.appspot.com/o/10-Lying-Windshield-Wipers.png?alt=media&token=159b8d1b-ceba-4315-9184-fd254f72329f",
    },
  },
];
