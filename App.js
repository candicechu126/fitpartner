import React from "react";
import { StyleSheet, Text, View } from "react-native";
import { SafeAreaProvider } from "react-native-safe-area-context";
import IntroScreen from "./screen/IntroScreen";
import { createSwitchNavigator, createAppContainer } from "react-navigation";
import LoginScreen from "./screen/loginSystem/LoginScreen";
import LoadingScreen from "./screen/loginSystem/LoadingScreen";
import DashboardScreen from "./screen/loginSystem/DashboardScreen";
import * as firebase from "firebase";
import { firebaseConfig } from "./config";
firebase.initializeApp(firebaseConfig);
// export default function App() {
//   return (
//     <SafeAreaProvider>
//       <IntroScreen />
//     </SafeAreaProvider>
//   );
// }

export default function App() {
  return <AppNavigator />;
}

const AppSwitchNavigator = createSwitchNavigator({
  LoadingScreen: LoadingScreen,
  LoginScreen: LoginScreen,
  DashboardScreen: DashboardScreen,
});

const AppNavigator = createAppContainer(AppSwitchNavigator);
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
});
